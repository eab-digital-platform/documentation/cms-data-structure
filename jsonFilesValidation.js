/**
 * Because many developers forget to check json files format in CB_resources
 * When we deploy to SIT3, it will cause error, and we need much time to roll back.
 * So we will check it on Gitlab pipeline and pre-commit husky when each commit, ensuring your json changes on CB_resources are correct.
 */
const fs = require('fs');
const glob = require('glob');
const validator = require('validator');

/**
 * @function
 * @description find out all json files under the path
 * @param  {string} path - the CB resources path. Should be "./CB_resources/files" in this project.
 * @returns array of files path
 */
const findOutAllJsonFiles = async (path) => {
    return new Promise((resolve, reject) => {
        glob(`${path}/**/**.json`, (err, files) => {
            if (err) {
                reject(err);
            } else {
                resolve(files);
            }
        });
    });
};

/**
 * @function
 * @description read file and return string (format: utf-8)
 * @param  {string} path - the file path. Such as "./CB_resources/files/appform_dyn_declaration_acc.json"
 * @returns content in the file with utf-8 format
 */
const readFile = async (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (readFileErr, result) => {
            if (readFileErr) {
                reject(readFileErr);
            } else {
                resolve(result);
            }
        });
    });
};

/**
 * @function
 * @description check if the string is json format
 * @param  {string} string - the string you want to check
 * @returns boolean value that if the string is json format
 */
const isJsonFormat = (string) => {
    if (!validator.isJSON(string)) {
        return false;
    }
    return true;
};

const checkIfJsonFilesAreValid = async() => {
    const allJsonFiles = await findOutAllJsonFiles('./').catch(err => {err;});
    if (allJsonFiles.err) {
        console.error(allJsonFiles.err);
        process.exit(1);
    } else {
        console.log(`Files total: ${allJsonFiles.length}`);
        for (var filePath of allJsonFiles) {
            const json = await readFile(filePath).catch(err => {err;});
            if (json.err) {
                console.error(`File ${filePath} can not be read.`);
                process.exit(1);
            } else {
                if (!isJsonFormat(json)) {
                    console.error(`File ${filePath} is not a json format file.`);
                    process.exit(1);
                }
            }
        }
    }
};

checkIfJsonFilesAreValid();


